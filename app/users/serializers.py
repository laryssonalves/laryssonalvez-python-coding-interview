from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    age = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = (
            'id', 'name', 'last_name', 'email', 'age',
        )