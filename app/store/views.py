from rest_framework.views import APIView
from rest_framework.response import Response
from app.store.models import Products
from app.store.serializers import ProductsSerializer


class ListProducts(APIView):
    def get(self, requrest):
        products = Products.objects.all()
        serializer = ProductsSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ProductsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def patch(self, request, pk):
        product = Products.objects.get(pk=pk)
        serializer = ProductsSerializer(product, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
